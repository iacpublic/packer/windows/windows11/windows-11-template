#iex ((new-object net.webclient).DownloadString('https://chocolatey.org/install.ps1'))
(new-object net.webclient).DownloadFile('https://chocolatey.org/install.ps1', 'C:\Windows\Temp\install.ps1')

$env:chocolateyUseWindowsCompression = 'false'
for($try = 0; $try -lt 5; $try++)
{
  C:/Windows/Temp/install.ps1
  
  #Install Tools with choco..
  choco install chocolatey-windowsupdate.extension -y	
  choco install spice-agent -y
  choco install virtio-drivers -y
  choco install keepass -y
  choco install putty -y
  choco install atom -y
  choco install firefox -y
  choco install 7zip.install -y
  choco install git.install -y
  choco install nodejs.install -y
  choco install cutepdf -y
  choco install javaruntime -y
  
  if ($?) { exit 0 }
  if (Test-Path C:\ProgramData\chocolatey) { exit 0 }
  Write-Host "Failed to install chocolatey (Try #${try})"
  Start-Sleep 2
}
Write-Error "Chocolatey failed to install, please re-build your machine again"
exit 2
