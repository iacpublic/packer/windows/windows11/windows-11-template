VMNAME="WIN11"
VMVERSION=$(date +"%m-%d-%y_%HH_%MM_%SS")
VMFULLNAME=$VMNAME'_'$VMVERSION
HDDEXT=".qcow2"
HDDEXTFILENAME=$VMFULLNAME$HDDEXT
VMDISKSTORE='/var/lib/libvirt/images/'
HDDFPATH=$VMDISKSTORE$HDDEXTFILENAME
DEFNETWORK='default'
sudo cp windows-11/windows-11 $HDDFPATH
sudo qemu-img resize $HDDFPATH +30G
sudo virt-install \
-n $VMFULLNAME \
--noautoconsole \
--description "VM Windows 11" \
--ram=2048 \
--vcpus=2 \
--os-variant=win10 \
--import \
--disk path=$HDDFPATH,bus=virtio,size=10 \
--graphics vnc,password=trustworthypass,listen=127.0.0.1,port=7499 \
--graphics spice,listen=127.0.0.1 \
--network network=$DEFNETWORK,model=virtio
